/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Button
} from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/screens/home'
import Book from './src/screens/book'
import Library from './src/screens/library'
import Store from '@/stores'

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    <Store>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
          <Stack.Screen name="Book" component={Book}
            options={styles.headerBook} 
          />
          <Stack.Screen name="Library" component={Library} options={styles.headerLibrary} />
        </Stack.Navigator>
      </NavigationContainer>
    </Store>
    
  );
};

const styles = {
  headerBook: {
    title: '',
    headerStyle: {
      elevation: 0,
      backgroundColor: '#FFD3B6'
    }
  },
  headerLibrary: {
    headerStyle: {
      elevation: 0,
      backgroundColor: '#F5F5F5'
    }
  }
};

export default App;
