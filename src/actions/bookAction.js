const getBooks = () => {
  const config = {
    method: 'GET'
  }

  return fetch('https://5ec39a02628c160016e70688.mockapi.io/api/v1/list_book', config)
    .then(res => {
      return res.json()
    })
    .catch(err => {
      return err
    })
}

export {
  getBooks
}