const getLibrary = () => {
  const config = {
    method: 'GET'
  }
  
  return fetch('https://5ec39a02628c160016e70688.mockapi.io/api/v1/library', config)
    .then(res => {
      return res.json()
    })
    .catch(err => {
      return err
    })
}

const addBookToLibrary = (data) => {
  const config = {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  }

  return fetch('https://5ec39a02628c160016e70688.mockapi.io/api/v1/library', config)
    .then(res => {
      return res.json()
    })
    .catch(err => {
      return err
    })
}

const removeBookFromLibrary = (id) => {
  const config = {
    method: 'Delete'
  }

  return fetch(`https://5ec39a02628c160016e70688.mockapi.io/api/v1/library/${id}`, config)
    .then(res => {
      return res.json()
    })
    .catch(err => {
      return err
    })
}

const updateBookToLibrary = (id, data) => {
  const config = {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  }

  return fetch(`https://5ec39a02628c160016e70688.mockapi.io/api/v1/library/${id}`, config)
    .then(res => {
      return res.json()
    })
    .catch(err => {
      return err
    })
}

export {
  getLibrary,
  addBookToLibrary,
  removeBookFromLibrary,
  updateBookToLibrary
}