import { types, flow } from 'mobx-state-tree'
import { 
  getLibrary, 
  addBookToLibrary, 
  removeBookFromLibrary,
  updateBookToLibrary
} from '../actions/libraryAction'

const Library = types.model('Library')
  .props({
    id: types.string,
    name: types.string,
    image: types.string,
    price: types.number,
    author: types.string,
    amount: types.string,
    bookId: types.string,
    description: types.string
  })

const LibraryStore = types.model('LibraryStore')
  .props({
    libraries: types.array(Library)
  })
  .actions(self => {
    const lstLibrary = flow(function*() {
      self.libraries = yield getLibrary()
      return self.libraries
    })

    const addBook = flow(function* (params) {
      yield addBookToLibrary(params)
    })

    const removeBook = flow(function* (id) {
      const book = yield removeBookFromLibrary(id)
      return self.libraries = self.libraries.filter(item => item.id !== book.id)
    })

    const updateBook = flow(function* (id, data) {
      const book = yield updateBookToLibrary(id, data)
      
      return self.libraries = self.libraries.map(item => {
        if (item.id === book.id)
          item.amount = book.amount
      })
    })
    
    return { 
      lstLibrary,
      addBook,
      removeBook,
      updateBook
    }
  })
export default LibraryStore.create({
  libraries: []
})

