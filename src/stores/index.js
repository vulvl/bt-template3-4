import React from 'react'
import { Provider } from 'mobx-react'
import libraryStore from './libraryStore'

const stores = {
  libraryStore
}

export default ({ children }) => (
  <Provider {...stores}>
    {children}
  </Provider>
)