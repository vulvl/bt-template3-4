import React, { Component } from 'react'
import { 
  View, 
  Text, 
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign';
import IconFeather from 'react-native-vector-icons/Feather';
import { getBooks } from '../actions/bookAction'

class Home extends Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    this.getListBook()
  }

  async getListBook() {
    const books = await getBooks()
    this.setState({ data: books })
  }

  render () {
    const { data } = this.state
    const { navigation } = this.props;

    return (
      <>
        <ScrollView style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.txtName}>Hi, Vulvl</Text>
            <Text style={styles.txtTitle}>Discover Latest Book</Text>
            <View style={styles.viewSearch}>
              <TextInput 
                style={styles.inputSearch}
                placeholder='Search book..'
              />
              <TouchableOpacity style={[styles.btnSearch, styles.commonBorderRaidus]}>
                <Icon name="search1" style={styles.iconSearch}/>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.menuBar}>
            <TouchableOpacity>
              <Text style={[styles.txtMenu, styles.textBold]}>New</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={[styles.txtMenu, styles.colorTextSilver]}>Trending</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={[styles.txtMenu, styles.colorTextSilver]}>Best Seller</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.lstImageBook}>
            <ScrollView 
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {  data.map(book => (
                <View key={book.id} style={styles.itemImg}>
                  <TouchableOpacity 
                    style={styles.btnShadow} 
                    onPress={() => navigation.navigate('Book', { book })}>
                    <Image 
                      style={[styles.imgBook, styles.commonBorderRaidus]}
                      source={{
                        uri: book.image,
                      }}
                    />
                  </TouchableOpacity>
                </View>
                ))
              }
            </ScrollView>
          </View>
          <Text style={styles.txtPopular}>Popular</Text>
          <View style={styles.lstBook}>
            {
              data.map(book => (
                <View key={book.id}>
                  <TouchableOpacity 
                    style={[styles.btnBook, 
                    styles.commonBorderRaidus]}
                    onPress={() => navigation.navigate('Book', { book })}
                  >
                    <Image 
                      source={{
                        uri: book.image,
                      }}
                      style={[styles.imgInfoBook, styles.commonBorderRaidus]}
                    />
                    <View style={styles.infoTxtBook}>
                      <Text style={styles.txtTitle}>{book.name}</Text>
                      <Text style={styles.txtAuthor}>{book.author}</Text>
                      <Text style={styles.txtPrice}>${book.price}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              ))
            }
          </View>
        
        </ScrollView>
        <View style={styles.footerMenu}>
          <TouchableOpacity>
            <IconFeather name='home' style={styles.iconMenu}/>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('Library')}
          >
            <IconFeather name='bookmark' style={styles.iconMenu}/>
          </TouchableOpacity>
          <TouchableOpacity>
            <IconFeather name='user' style={styles.iconMenu}/>
          </TouchableOpacity>
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: '#fff'
  },
  header: {
    marginTop: 40,
    height: 130,
  },
  txtName: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#AAAAAA',
    fontWeight: 'bold',
    marginBottom: 5
  },
  txtTitle: {
    fontSize: 22,
    fontFamily: 'OpenSans-SemiBold',
    fontWeight: '600'
  },
  viewSearch: {
    flexDirection: 'row',
    marginTop: 20
  },
  inputSearch: {
    height: 39,
    width: 330,
    backgroundColor: '#F4F4F4',
    paddingLeft: 19,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  commonBorderRaidus: {
    borderRadius: 10
  },
  btnSearch: {
    backgroundColor: '#FFAAA5',
    width: 41,
    height: 39,
    position: 'absolute',
    left: 320,
    justifyContent: 'center',
  },
  iconSearch: {
    color: '#fff',
    fontSize: 22,
    textAlign: 'center'
  },
  menuBar: {
    flexDirection: 'row',
    marginBottom: 10
  },
  txtMenu: {
    fontFamily: 'OpenSans-Regular',
    marginRight: 23,
    fontSize: 12
  },
  lstImageBook: {
    flexDirection: 'row',
    height: 270
  },
  imgBook: {
    height: 250,
    width: 170,
  },
  btnShadow: {
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#FFE2E0',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 11,
  },
  itemImg: {
    marginRight: 20,
  },
  txtPopular: {
    fontSize: 21,
    fontFamily: 'OpenSans-SemiBold',
    marginTop: 20
  },
  lstBook: {
    flexDirection: 'column',
    marginTop: 20,
  },
  imgInfoBook: {
    height: 81,
    width: 62,
    resizeMode: 'contain',
    marginLeft: 13,
  },
  infoTxtBook: {
    flexDirection: 'column',
    height: 81,
    width: 230,
    marginLeft: 21,
    marginTop: 11
  },
  txtTitle: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    marginBottom: 5
  },
  txtAuthor: {
    fontSize: 10,
    marginBottom: 5,
    color: '#AAAAAA',
    fontFamily: 'OpenSans-SemiBold',
  },
  txtPrice: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    marginBottom: 5
  },
  btnBook: {
    height: 100,
    marginBottom: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 2.62,

    elevation: 6,
    flexDirection: 'row',
    width: 350,
    marginLeft: 5
  },
  footerMenu: {
    height: 80,
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#fff',
    alignItems: 'center'
  },
  iconMenu: {
    fontSize: 24,
    color: '#AAAAAA'
  },
  colorTextSilver: {
    color: '#AAAAAA'
  },
  textBold: {
    fontWeight: 'bold'
  }
})

export default Home