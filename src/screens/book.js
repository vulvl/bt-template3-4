import React, { Component } from 'react'
import { 
  View, 
  Text, 
  StyleSheet, 
  Image, 
  ScrollView ,
  TouchableOpacity
} from 'react-native'
import { inject, observer } from 'mobx-react'
import { addBookToLibrary } from '../actions/libraryAction'

@inject((stores) => ({
  libraryStore: stores.libraryStore
}))

@observer
class Book extends Component {
  
  _addBookToLibrary = async () => {
    const { navigation, route, libraryStore } = this.props;
    const book = route.params.book
    const paramsBook = {
      name: book.name,
      price: book.price,
      image: book.image,
      description: book.description,
      author: book.author,
    	amount: "1",
    	bookId: book.id
    }
    
    await libraryStore.addBook(paramsBook)
    navigation.navigate('Library')
  }

  render() {
    const { route } = this.props
    const book = route.params.book
    
    return (
      <>
        <ScrollView style={styles.container}>
          <View style={styles.containerTop}>
            <View style={styles.bookImg}>
              <Image 
                source={{ uri: book.image }}
                style={styles.imgBook}
              />
            </View>
            <View style={styles.bookInfo}>
              <Text style={styles.txtTitle}>{book.name}</Text>
              <Text style={styles.txtAuthor}>{book.author}</Text>
              <View style={styles.viewPrice}>
                <Text style={styles.txtMoney}>$</Text>
                <Text style={styles.txtPrice}>{book.price}</Text>
              </View>
            </View>
          </View>
          <View style={styles.containerBottom}>
            <View style={styles.menu}>
              <TouchableOpacity>
                <Text style={[styles.txtMenu, styles.textBold]}>Description</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={[styles.txtMenu, styles.colorTextSilver]}>Reviews</Text>
              </TouchableOpacity>
              <TouchableOpacity>
                <Text style={[styles.txtMenu, styles.colorTextSilver]}>Similliar</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.infoBook}>
              <Text style={styles.txtDescrip}>{book.description}</Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.btnAddToLibrary}>
          <TouchableOpacity 
            style={styles.btnLibrary} 
            onPress={this._addBookToLibrary}
          >
            <Text style={styles.txtAdd}>Add to Library</Text>
          </TouchableOpacity>
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerTop: {
    flexDirection: 'column'
  },
  containerBottom: {
  },
  bookImg: {
    backgroundColor: '#FFD3B6',
    justifyContent: 'center',
    alignItems: 'center',
    height: 341
  },
  bookInfo: {
    height: 130,
    marginLeft: 25,
    marginTop: 24
  },
  imgBook: {
    width: 172,
    height: 225,
    resizeMode: 'contain',
    borderRadius: 10
  },
  txtTitle: {
    fontSize: 27,
    fontFamily: 'OpenSans-SemiBold'
  },
  txtAuthor: {
    fontSize: 14,
    marginTop: 7,
    color: '#AAAAAA'
  },
  viewPrice: {
    flexDirection: 'row'
  },
  txtPrice: {
    fontSize: 32,
    marginTop: 5,
    color: '#FFAAA5',
    fontFamily: 'OpenSans-SemiBold'
  },
  txtMoney: {
    marginTop: 7,
    fontSize: 14,
    color: '#FFAAA5',
    fontFamily: 'OpenSans-Regular'
  },
  menu: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  txtMenu: {
    fontSize: 14,
    fontFamily: 'OpenSans-SemiBold',
  },
  infoBook: {
    height: 200,
    marginTop: 26,
    marginLeft: 25,
    marginRight: 25
  },
  txtDescrip: {
    fontSize: 12,
    fontFamily: 'OpenSans-Regular',
    color: '#AAAAAA',
    lineHeight: 25,
  },
  btnLibrary: {
    width: 353,
    height: 49,
    backgroundColor: '#FFAAA5',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnAddToLibrary: {
    marginBottom: 30,
    marginLeft: 25,
    marginRight: 25
  },
  txtAdd: {
    color: '#fff',
    fontSize: 14,
    fontFamily: 'OpenSans-SemiBold',
  },
  colorTextSilver: {
    color: '#AAAAAA'
  },
  textBold: {
    fontWeight: 'bold'
  }
})

export default Book