import React, { Component } from 'react'
import { 
  View, 
  Text, 
  StyleSheet, 
  ScrollView, 
  TouchableOpacity,
  Image,
  TextInput
} from 'react-native'
// import { getLibrary } from '../actions/libraryAction'
import Icon from 'react-native-vector-icons/AntDesign';
import { inject, observer } from 'mobx-react'

@inject((stores) => ({
  libraryStore: stores.libraryStore
}))

@observer
class Library extends Component {
  constructor () {
    super()
    this.state = {
      quality: {
        id: '',
        amount: ''
      }
    }
  }
  
  componentDidMount () {
    this._getLibrary()
  }

  async _getLibrary () {
    const { libraryStore } = this.props
    await libraryStore.lstLibrary()
  }

  async _onChangeAmount (params) {
    const { libraryStore } = this.props
    let { quality } = this.state
    this.setState({ quality: params})

    if (!params.amount)
      quality.amount = '1'
    
    if (params.amount)
      await libraryStore.updateBook(params.id, { amount: params.amount })
  }
  
  async _onDelete (id) {
    const { libraryStore } = this.props
    await libraryStore.removeBook(id)
  }
  
  async _plusAmount(id, amount, type) {
    const { libraryStore } = this.props
    let amountNumber = parseInt(amount)

    if (type === 'plus')
      amountNumber++
    else  
      amountNumber--

    if (amountNumber === 0)
      amountNumber = 1

    const amountChanged = amountNumber.toString()
    
    await libraryStore.updateBook(id, { amount: amountChanged })
  }

  _valueAmount (id, amount) {
    const { quality } = this.state
    if (id == quality.id)
      return quality.amount

    return amount
  }

  render() {
    const { navigation, libraryStore } = this.props
    const { libraries } = libraryStore

    let total = 0
    libraries.map(book => {
      total += book.price * parseInt(book.amount)
    })
    
    return (
      <>
        <ScrollView style={styles.containerScroll}>
          <View style={styles.background}>
            <View style={styles.lstBook}>
              { libraries && libraries.map(book => (
                <View key={book.id}>
                  <TouchableOpacity 
                    style={styles.btnBook}
                    onPress={() => navigation.navigate('Book', { book })}
                  >
                    <Image source={{ uri: book.image}} style={styles.imgBook}/>
                    <View style={styles.infoBook}>
                      <Text style={styles.txtTitle}>{book.name}</Text>
                      <Text style={styles.txtAuthor}>{book.author}</Text>
                      <Text style={styles.txtPrice}>${book.price}</Text>
                      <View style={styles.btnsAction}>
                        <View style={styles.btnsAmount}>
                          <TouchableOpacity 
                            style={styles.btnPlus}
                            onPress={() => this._plusAmount(book.id, book.amount, 'plus')}
                          >
                            <Icon name='plus'/>
                          </TouchableOpacity>
                          <TextInput 
                            style={styles.iptAmount}
                            keyboardType={'numeric'}
                            value={this._valueAmount(book.id, book.amount)}
                            onChangeText={text => this._onChangeAmount({ id: book.id, amount: text, })}
                          />
                            <TouchableOpacity 
                              style={styles.btnPlus}
                              onPress={() => this._plusAmount(book.id, book.amount, 'minus')}
                            >
                            <Icon name='minus'/>
                          </TouchableOpacity>
                        </View>
                        <View style={styles.viewClose}>
                          <TouchableOpacity 
                            style={styles.btnClose}
                            onPress={() => this._onDelete(book.id)}
                          >
                            <Icon name='close' style={styles.iconClose}/>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>
        </ScrollView>
        <View style={styles.btnBuy}>
          <TouchableOpacity style={styles.btnLibrary}>
            <Text style={styles.txtAdd}>BUY {total ? `$${total}` : null}</Text>
          </TouchableOpacity>
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  containerScroll: {
    flex: 1,
  },
  background: {
    backgroundColor: '#FFD3B6',
    borderRadius: 110,
    marginTop: 30,
    flexDirection: 'column'
  },
  lstBook: {
    marginLeft: 20,
    marginRight: 20,
  },
  imgBook: {
    height: 150,
    width: 80,
    resizeMode: 'contain',
    marginLeft: 13,
    borderRadius: 10
  },
  btnBook: {
    backgroundColor: '#fff',
    height: 150,
    marginBottom: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 2.62,

    elevation: 6,
    flexDirection: 'row',
    marginLeft: 5,
    borderRadius: 10
  },
  infoBook: {
    flexDirection: 'column',
    marginLeft: 20,
    marginTop: 10,
  },
  txtTitle: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 20,
    marginBottom: 5
  },
  txtAuthor: {
    fontSize: 14,
    marginBottom: 5,
    color: '#AAAAAA',
    fontFamily: 'OpenSans-SemiBold',
  },
  txtPrice: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 26,
    marginBottom: 5,
    color: '#FFAAA5'
  },
  btnBuy: {
    marginBottom: 30,
    marginLeft: 25,
    marginRight: 25
  },
  txtAdd: {
    color: '#fff',
    fontSize: 16,
    fontFamily: 'OpenSans-SemiBold',
  },
  btnLibrary: {
    width: 367,
    height: 49,
    backgroundColor: '#FFAAA5',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnsAction: {
    marginTop: 8,
    flexDirection: 'row'
  },
  btnsAmount: {
    flexDirection: 'row',
  },
  btnPlus: {
    backgroundColor: '#D3D3D3',
    height: 21,
    width: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  iptAmount: {
    height: 50,
    marginTop: -15,
    textAlign: 'center'
  },
  viewClose: {
    height: 21,
    width: 150
  },
  btnClose: {
    width: 20,
    height: 21,
    alignSelf: 'flex-end',
    justifyContent: 'center'
  },
  iconClose: {
    textAlign: 'center'
  }
})

export default Library